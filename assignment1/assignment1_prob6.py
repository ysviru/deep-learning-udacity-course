# Assignment 1: Problem 6

# --------------------------------------------------------------------------------------------------
# Part 1 : Train a simple model on this data using a 50, 100, 1000 and 5000 training samples. 
#          (Hint: you can use the LogisticRegression model from sklearn.linear_model)
# --------------------------------------------------------------------------------------------------

import string 
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import os
import sys
import tarfile
from IPython.display import display, Image
from scipy import ndimage
from sklearn.linear_model import LogisticRegression
from six.moves.urllib.request import urlretrieve
from six.moves import cPickle as pickle
import time

# Read data (stored as a map) from file (saved to disk in Problem 4). 
def extract_data(filename): 
	try: 
		file_obj = open(filename, 'r')
		return pickle.load(file_obj)
	except Exception as e: 
		print "Unable to process file " + filename + ": " + str(e) 
		raise

# Using sklearn.linear_model tool, we fit a model using logistic regression using our training data. 
# We later on use this model to make predictions and we quantify the accuracy of the model using existing methods. 
def get_trained_logistic_regression_model(train_dataset, train_labels, num_data_train): 
	X = train_dataset[0:num_data_train, :, :]
	Y = train_labels[0:num_data_train]
	# Reshape to convert from 3D to a 2D matrix. 
	# This is required for logistic regression package from sklearn.linear_model
	X = np.reshape(X, (X.shape[0], X.shape[1] * X.shape[2]))

	# Initialize LogisticRegression model. 
	logregmodel = LogisticRegression(C=1e5)

	# Fit or train the model. 
	t0 = time.time()
	logregmodel.fit(X, Y)
	t1 = time.time()
	elapsed_time = t1-t0
	print "Time to fit model: " + str(elapsed_time) + " seconds."

	return logregmodel

def test_model_and_score(logregmodel, test_dataset, test_labels):
	X2 = test_dataset
	# Reshape to convert from 3D to a 2D matrix. 
	# This is required for logistic regression package from sklearn.linear_model
	X2 = np.reshape(X2, (X2.shape[0], X2.shape[1] * X2.shape[2]))
	Y2 = test_labels
	'''
	# The predict function is useful, if we are interested in using the predicted class information in different ways. 
	Y2 = logregmodel.predict(X2)
	dim1 = X2.shape[0]
	num_correct = 0
	for i in range(0, dim1):
		if Y2[i] == test_labels[i]: 
			num_correct = num_correct + 1
	print "percentage accuracy = " + str(float(num_correct)/float(dim1) * 100) + " with " + str(num_data_train) + " training samples"
	'''
	
	# If we are only interested in how well we are doing--- 
	# as opposed to making the calculations above we could simply use the score function as follows, 
	return logregmodel.score(X2, Y2)


filename = 'notMNIST.pickle'
data = extract_data(filename)
# Training and testing datasets. 
train_dataset = data['train_dataset']
train_labels = data['train_labels']
test_dataset = data['test_dataset']
test_labels = data['test_labels']

num_data_train = 100

print "Training logistic regression model on data..."
logregmodel = get_trained_logistic_regression_model(train_dataset, train_labels, num_data_train)

print "Running trained model on test data and computing score..."
print "Fraction of test data correctly classified = " + str(test_model_and_score(logregmodel, test_dataset, test_labels)) 
