# Assignment 1: Problem 2

# ----------------------------------------------------------------------------
# Convince yourself that the stored ndarray correctly represents the data. 
# To do this we can visualize the data using matplotlib.pyplot
# ----------------------------------------------------------------------------

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from six.moves import cPickle as pickle
import numpy as np

# 1. Open a sample .pickle file. 
filepath = 'notMNIST_large/A.pickle'
try: 
	file_obj = open(filepath, 'r')

	# 2. Use pickle class to load the pickle file. 
	A_dataset = pickle.load(file_obj)
	print A_dataset.shape

	# 3. Use matplotlob to plot a sample image and then compare with existing database. 
	plt.imshow(A_dataset[5,:,:])
	plt.show()

except Exception as e: 
	print 'Unable to process file ' + filepath + ': ' +str(e) 

# ------------
# Findings: 
# ------------
# Visually comparing the data set for letter A to the existing database, we see that there is no change. 
# I am convinced! :)
