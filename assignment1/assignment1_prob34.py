# ----------------------------------------------------------------
# Assignment 1: Problem 3, 4
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# Part 1: Verify that the data is balanced across classes. 

# Part 2: Merge and prune the training data as needed. Depending on your computer setup, you might not be able to fit it all in memory, and you can tune train_size as needed. 
#         The labels will be stored into a separate array of integers 0 through 9.
#         Also create a validation dataset for hyperparameter tuning.

# Part 3: Next, we'll randomize the data. It's important to have the labels well shuffled for the training and test distributions to match.

# Part 4: Convince yourself, that the data is good after shuffling. Looking at the code, this seems trivially true! 
#         Store the data to a file on disk. 
# ----------------------------------------------------------------
# ----------------------------------------------------------------

import string 
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import os
import sys
import tarfile
from IPython.display import display, Image
from scipy import ndimage
from sklearn.linear_model import LogisticRegression
from six.moves.urllib.request import urlretrieve
from six.moves import cPickle as pickle



# --------------------------------------------------------------------------------------------------
# Part 1: To verify, we count the number the samples in the training set for each of the classes. 
#         We want to make sure no class is under/over represented. 
# --------------------------------------------------------------------------------------------------
def get_num_samples(dataset): 
	# the first dimension represents the number of samples. 
	return dataset.shape[0]

def get_list_letters(letter1, letter2): 
	letters = list(map(chr, range(ord(letter1), ord(letter2)+1)))
	return letters

def get_filenames(letters, type): 
	
	num_letters = len(letters)
	flag = True
	file_names = [] 
	if type == 'train':
		file_names = ['notMNIST_large/'] * num_letters
	elif type == 'test':
		file_names = ['notMNIST_small/'] * num_letters
	else:
		print "(ERROR) Illegal option."
		flag = False
	
	if flag:
		for i in range(0, num_letters):
			file_names[i] += letters[i] + ".pickle"
	
	return file_names



def print_num_samples_all_datasets(): 
	letters = get_list_letters('A', 'J')
	file_names = get_filenames(letters, 'train')

	for i in range(0, len(file_names)): 
		try: 
			# 1. Open a sample .pickle file. 
			file_obj = open(file_names[i], 'r')
			# 2. Use pickle class to load the pickle file. 
			dataset = pickle.load(file_obj)
			# 3. Print number of samples. 
			print "For letter " + letters[i] + ", number of samples = " + str(get_num_samples(dataset))
		except Exception as e: 
			print "Unable to process file " + file_names[i] + ": " + str(e) 
			raise

# Printing the number of samples shows that all classes have roughly the same number of samples in the training set. 
print "Num samples per class..."
print_num_samples_all_datasets() 
print 
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
# ---------------------------r-----------------------------------------------------------------------


# --------------------------------------------------------------------------------------------------
# Part 2: 
# --------------------------------------------------------------------------------------------------

image_size = 28  # Pixel width and height.
pixel_depth = 255.0  # Number of levels per pixel.



def make_arrays(nb_rows, img_size):
  if nb_rows:
    dataset = np.ndarray((nb_rows, img_size, img_size), dtype=np.float32)
    labels = np.ndarray(nb_rows, dtype=np.int32)
  else:
    dataset, labels = None, None
  return dataset, labels

def merge_datasets(pickle_files, train_size, valid_size=0):
  num_classes = len(pickle_files)
  valid_dataset, valid_labels = make_arrays(valid_size, image_size)
  train_dataset, train_labels = make_arrays(train_size, image_size)
  vsize_per_class = valid_size // num_classes
  tsize_per_class = train_size // num_classes
    
  start_v, start_t = 0, 0
  end_v, end_t = vsize_per_class, tsize_per_class
  end_l = vsize_per_class+tsize_per_class
  for label, pickle_file in enumerate(pickle_files):       
    try:
      with open(pickle_file, 'rb') as f:
        letter_set = pickle.load(f)
        # let's shuffle the letters to have random validation and training set
        np.random.shuffle(letter_set)
        if valid_dataset is not None:
          valid_letter = letter_set[:vsize_per_class, :, :]
          valid_dataset[start_v:end_v, :, :] = valid_letter
          valid_labels[start_v:end_v] = label
          start_v += vsize_per_class
          end_v += vsize_per_class
                    
        train_letter = letter_set[vsize_per_class:end_l, :, :]
        train_dataset[start_t:end_t, :, :] = train_letter
        train_labels[start_t:end_t] = label
        start_t += tsize_per_class
        end_t += tsize_per_class
    except Exception as e:
      print('Unable to process data from', pickle_file, ':', e)
      raise
    
  return valid_dataset, valid_labels, train_dataset, train_labels
            
            
train_size = 200000
valid_size = 10000
test_size = 10000

letters = get_list_letters('A', 'J')

train_datasets = get_filenames(letters, 'train')
test_datasets =  get_filenames(letters, 'test')


valid_dataset, valid_labels, train_dataset, train_labels = merge_datasets(
  train_datasets, train_size, valid_size)
_, _, test_dataset, test_labels = merge_datasets(test_datasets, test_size)

print('Training:', train_dataset.shape, train_labels.shape)
print('Validation:', valid_dataset.shape, valid_labels.shape)
print('Testing:', test_dataset.shape, test_labels.shape)

# Prints all the data in the train set. 
print 
print train_dataset
print

# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------



# --------------------------------------------------------------------------------------------------
# Part 3: 
# --------------------------------------------------------------------------------------------------



def randomize(dataset, labels):
	permutation = np.random.permutation(labels.shape[0])
	shuffled_dataset = dataset[permutation,:,:]
	shuffled_labels = labels[permutation]
	return shuffled_dataset, shuffled_labels

print "Creating a shuffling of the data in each set..."
train_dataset, train_labels = randomize(train_dataset, train_labels)
test_dataset, test_labels = randomize(test_dataset, test_labels)
valid_dataset, valid_labels = randomize(valid_dataset, valid_labels)

# Compare the following shuffling with the previously printed train_dataset. 
print 
print train_dataset
print 



# --------------------------------------------------------------------------------------------------
# Part 4: 
# --------------------------------------------------------------------------------------------------

pickle_file = 'notMNIST.pickle'

try:
	f = open(pickle_file, 'wb')
	save = { 'train_dataset': train_dataset, 'train_labels': train_labels, 'valid_dataset': valid_dataset, 'valid_labels': valid_labels, 'test_dataset': test_dataset, 'test_labels': test_labels,
    }
	pickle.dump(save, f, pickle.HIGHEST_PROTOCOL)
	f.close()
except Exception as e:
	print('Unable to save data to', pickle_file, ':', e)
	raise

print 
statinfo = os.stat(pickle_file)
print('Compressed pickle size:', statinfo.st_size)
print 





