# Assignment 1: Problem 5

# --------------------------------------------------------------------------------------------------
# Part 1 : Measure how much overlap there is between training, validation and test samples. 


# TODO
# PART 2: (Optional) What about near duplicates between datasets? (i.e., images that are 
#         almost identical)

# TODO
# PART 3: (Optional) Create a sanitized validation and test set, and compare your accuracy on 
#         those in subsequent assignments
# --------------------------------------------------------------------------------------------------


import string 
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import os
import sys
import tarfile
from IPython.display import display, Image
from scipy import ndimage
from sklearn.linear_model import LogisticRegression
from six.moves.urllib.request import urlretrieve
from six.moves import cPickle as pickle
import time



# Read data (stored as a map) from file (saved to disk in Problem 4). 
def extract_data(filename): 
	try: 
		file_obj = open(filename, 'r')
		return pickle.load(file_obj)
	except Exception as e: 
		print "Unable to process file " + filename + ": " + str(e) 
		raise




def get_number_of_same_images(dataset1, dataset2): 
	num_same_images = 0
	# Number of images in dataset1
	dim1 = dataset1.shape[0]
	# Number of images in dataset2
	dim2 = dataset2.shape[0]
	
	for i in range(0, dim1):
		t0 = time.time()
		for j in range(0, dim2):
			if ((dataset1[i] == dataset2[j]).all()):
				num_same_images = num_same_images + 1
				print "num_same_images = " + str(num_same_images)
				# We do not count duplicates in dataset2 that may match with dataset1
				# We hence, use the break statement in this case. (This may be handled as a loop condition, if you hate break statements.)
				break

		t1 = time.time()
		total = t1-t0
		print "i = " + str(i) + "\ttime elapsed = " + str(total)
	return num_same_images

# Gives the percentage of data in dataset1 that overlaps with dataset2
def get_percent_overlap(dataset1, dataset2): 	
	num_dataset1 = dataset1.shape[0]
	percent_overlap = float(get_number_of_same_images(dataset1, dataset2)) / float(num_dataset1) 
	return percent_overlap

filename = 'notMNIST.pickle'

data = extract_data(filename)

train_dataset = data['train_dataset']
test_dataset = data['test_dataset']
valid_dataset = data['valid_dataset']

print train_dataset.shape
print test_dataset.shape
print valid_dataset.shape


# Overlap can be bad-- we may fool ourselves into thinking we have a good model that 
#                      may not do well when run on other test data, the model has never seen. 

# Find overlap between training and test 
print "percentage of train dataset samples found in test dataset = " + str(get_percent_overlap(train_dataset, test_dataset))


# Find overlap between training and validation. 
print "percentage of train dataset samples found in validation dataset = " + str(get_percent_overlap(train_dataset, valid_dataset))



# ----------------------------------------------------------------------------------------------
# PART 2: (Optional) 
# ----------------------------------------------------------------------------------------------




# ----------------------------------------------------------------------------------------------
# PART 3: (Optional)
# ----------------------------------------------------------------------------------------------





