# ------------------------------------------------------------------------------------------------------------
# Assignment 3: Logistic Regression with regularization using L2 loss term. 
# ------------------------------------------------------------------------------------------------------------

# ------------------------------------------------------------------------------------------------------------
# NOTES: 
# ------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------
# General form: loss = loss + \beta * l2_loss(weights)
# We have one more hyperparameter to tune, i.e., \beta. 
# In the following piece of code, we tried different values of beta and got the following results: 

# \beta           Validation Accuracy      Test Accuracy

# 0.1             77%                      83.7%
# 0.05            79.2%                    86.2%
# 0.01            81%                      88% 
# 0.005           81.4%                    88.4%
# 0.001           81.7%                    88.8%          
# 0.0005          80.3%                    87.7%
# 0.0001          78.3%                    86.3%

# From the above summary, beta = 0.001 seems to give the right amount of regularization 
# scoring highest validation and test accuracies. 
# ------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------

from __future__ import print_function
import numpy as np
import tensorflow as tf 
from six.moves import cPickle as pickle
from six.moves import range

import time

# ---------------------------------------------------------------------------------
# Loading data stored in .pickle format in Assignment 1. 
# ---------------------------------------------------------------------------------
t0 = time.time()
pickle_file = 'notMNIST.pickle'

with open(pickle_file, 'rb') as f:
	save = pickle.load(f)
	train_dataset = save['train_dataset']
	train_labels = save['train_labels']
	valid_dataset = save['valid_dataset']
	valid_labels = save['valid_labels']
	test_dataset = save['test_dataset']
	test_labels = save['test_labels']
	del save  # hint to help gc free up memory
	print('Training set', train_dataset.shape, train_labels.shape)
	print('Validation set', valid_dataset.shape, valid_labels.shape)
	print('Test set', test_dataset.shape, test_labels.shape)

t1 = time.time()
elapsed_time = t1-t0 
print('Time to load data = ', str(elapsed_time), " sec.")
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------


# Reformatting data into shape that is amenable to train. 
# ---------------------------------------------------------------------------------
t0 = time.time()

image_size = 28
num_labels = 10

def reformat(dataset, labels): 
	dataset = dataset.reshape((-1, image_size*image_size)).astype(np.float32)
	labels = (np.arange(num_labels) == labels[:,None]).astype(np.float32)
	return dataset, labels

train_dataset, train_labels = reformat(train_dataset, train_labels)
valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
test_dataset, test_labels = reformat(test_dataset, test_labels)
print('Training set', train_dataset.shape, train_labels.shape)
print('Validation set', valid_dataset.shape, valid_labels.shape)
print('Test set', test_dataset.shape, test_labels.shape)

t1 = time.time()
elapsed_time = t1-t0 
print("Time to reformat data = ", str(elapsed_time), " sec.")
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------

# ------------------
# Accuracy function 
# ------------------
def accuracy(predictions, labels):
	return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
          / predictions.shape[0])
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------
# Construct the computation graph: Stochastic gradient descent method. 
# ---------------------------------------------------------------------------------
t0 = time.time()
batch_size = 128
beta = 0.001

graph = tf.Graph()
with graph.as_default():

	# Input data. For the training data, we use a placeholder that will be fed
	# at run time with a training minibatch.
	tf_train_dataset = tf.placeholder(tf.float32,
                                    shape=(batch_size, image_size * image_size))
	tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
	tf_valid_dataset = tf.constant(valid_dataset)
	tf_test_dataset = tf.constant(test_dataset)
  
	# Variables.
	weights = tf.Variable(
		tf.truncated_normal([image_size * image_size, num_labels]))
	biases = tf.Variable(tf.zeros([num_labels]))
  
	# Training computation.
	logits = tf.matmul(tf_train_dataset, weights) + biases
	loss = tf.reduce_mean(
		tf.nn.softmax_cross_entropy_with_logits(logits, tf_train_labels)) + (beta)*tf.nn.l2_loss(weights)
  
	# Optimizer.
	optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss) 
  
	# Predictions for the training, validation, and test data.
	train_prediction = tf.nn.softmax(logits)
	valid_prediction = tf.nn.softmax(
		tf.matmul(tf_valid_dataset, weights) + biases)
	test_prediction = tf.nn.softmax(tf.matmul(tf_test_dataset, weights) + biases)


t1 = time.time() 
elapsed_time = t1-t0
print('Time to construct computation graph (stochastic gradient descent) = ', str(elapsed_time), " sec.")
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------



# ---------------------------------------------------------------------------------
# RUN THE GRAPH COMPUTATION: Stochastic gradient descent method. 
# ---------------------------------------------------------------------------------
t0 = time.time() 
num_steps = 3001

with tf.Session(graph=graph) as session:
	tf.global_variables_initializer().run()
	print("Initialized")
	for step in range(num_steps):
		# Pick an offset within the training data, which has been randomized.
		# Note: we could use better randomization across epochs.
		offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
		# Generate a minibatch.
		batch_data = train_dataset[offset:(offset + batch_size), :]
		batch_labels = train_labels[offset:(offset + batch_size), :]
		# Prepare a dictionary telling the session where to feed the minibatch.
		# The key of the dictionary is the placeholder node of the graph to be fed,
		# and the value is the numpy array to feed to it.
		feed_dict = {tf_train_dataset : batch_data, tf_train_labels : batch_labels}
		_, l, predictions = session.run(
			[optimizer, loss, train_prediction], feed_dict=feed_dict)
		if (step % 500 == 0):
			print("Minibatch loss at step %d: %f" % (step, l))
			print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
			print("Validation accuracy: %.1f%%" % accuracy(
				valid_prediction.eval(), valid_labels))
	print("Test accuracy: %.1f%%" % accuracy(test_prediction.eval(), test_labels))


t1 = time.time()
elapsed_time = t1-t0
print("Time to run the computation graph (stochastic gradient descent) = ", str(elapsed_time), " sec.")
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------


