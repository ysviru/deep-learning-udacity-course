# -------------------------------------------------------------------------------------------------------
# Assignment 2: Given code to train logistic regression using stochastic gradient descent. 
# -------------------------------------------------------------------------------------------------------

# THE CODE IN THIS FILE IS GIVEN TO US. 
# It uses stochastic gradient descent to train multinomial logistic regression. 
# It also has code to traing the regression method using gradient descent. This is currently commented out. 
# To test its performance, we simply add basic timig code to get a ballpark.  

# ----------
# FINDINGS:
# ----------
# The maximum possible accuracy for validation ~ 82%  and for test ~ 89% 
# This does not include regularization. We only varying learning rate and number of epochs. 



from __future__ import print_function
import numpy as np
import tensorflow as tf
from six.moves import cPickle as pickle
from six.moves import range

import time
import sys

sys.path.insert(1, '../helperfiles/')

import global_variables as gv
import datafiles as df
import tfsession

# ---------------------------------------------------------------------------------
# Loading data stored in .pickle format in Assignment 1. 
# ---------------------------------------------------------------------------------
train_dataset, train_labels, valid_dataset, valid_labels, test_dataset, test_labels = df.load_data(gv.pickle_file)
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------
# Reformatting data into shape that is amenable to train. 
# ---------------------------------------------------------------------------------
t0 = time.time()

train_dataset, train_labels = df.reformat(train_dataset, train_labels, gv.image_size, gv.num_labels)
valid_dataset, valid_labels = df.reformat(valid_dataset, valid_labels, gv.image_size, gv.num_labels)
test_dataset, test_labels = df.reformat(test_dataset, test_labels, gv.image_size, gv.num_labels)
print('Training set', train_dataset.shape, train_labels.shape)
print('Validation set', valid_dataset.shape, valid_labels.shape)
print('Test set', test_dataset.shape, test_labels.shape)

t1 = time.time()
elapsed_time = t1-t0 
print("Time to reformat data = ", str(elapsed_time), " sec.")
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------


'''
# With gradient descent training, even this much data is prohibitive.
# Subset the training data for faster turnaround.

# ---------------------------------------------------------------------------------
# Construct the computation graph: Gradient descent method. 
# ---------------------------------------------------------------------------------
t0 = time.time()
train_subset = 10000

graph = tf.Graph()

with graph.as_default():

	# Input data.
	# Load the training, validation and test data into constants that are
	# attached to the graph.
	tf_train_dataset = tf.constant(train_dataset[:train_subset, :])
	tf_train_labels = tf.constant(train_labels[:train_subset])
	tf_valid_dataset = tf.constant(valid_dataset)
	tf_test_dataset = tf.constant(test_dataset)
  
	# Variables.
	# These are the parameters that we are going to be training. The weight
	# matrix will be initialized using random values following a (truncated)
	# normal distribution. The biases get initialized to zero.
	weights = tf.Variable(
    	tf.truncated_normal([gv.image_size * gv.image_size, gv.num_labels]))
  	biases = tf.Variable(tf.zeros([gv.num_labels]))
  
	# Training computation.
	# We multiply the inputs with the weight matrix, and add biases. We compute
	# the softmax and cross-entropy (it's one operation in TensorFlow, because
	# it's very common, and it can be optimized). We take the average of this
	# cross-entropy across all training examples: that's our loss.
	logits = tf.matmul(tf_train_dataset, weights) + biases
	loss = tf.reduce_mean(
		tf.nn.softmax_cross_entropy_with_logits(logits, tf_train_labels))
  
	# Optimizer.
	# We are going to find the minimum of this loss using gradient descent.
	optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss)
  
	# Predictions for the training, validation, and test data.
	# These are not part of training, but merely here so that we can report
	# accuracy figures as we train.
	train_prediction = tf.nn.softmax(logits)
	valid_prediction = tf.nn.softmax(
		tf.matmul(tf_valid_dataset, weights) + biases)
	test_prediction = tf.nn.softmax(tf.matmul(tf_test_dataset, weights) + biases)


t1 = time.time()
elapsed_time = t1-t0 
print("Time to construct computation graph (gradient descent) = ", str(elapsed_time), " sec.")
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------



# ---------------------------------------------------------------------------------
# RUN THE GRAPH COMPUTATION: Gradient descent method. 
# ---------------------------------------------------------------------------------
t0 = time.time()
num_steps = 801


with tf.Session(graph=graph) as session:
	# This is a one-time operation which ensures the parameters get initialized as
	# we described in the graph: random weights for the matrix, zeros for the
	# biases. 
	tf.global_variables_initializer().run()
	print('Initialized')
	for step in range(num_steps):
		# Run the computations. We tell .run() that we want to run the optimizer,
		# and get the loss value and the training predictions returned as numpy
		# arrays.
		_, l, predictions = session.run([optimizer, loss, train_prediction])
		if (step % 100 == 0):
			print('Loss at step %d: %f' % (step, l))
			print('Training accuracy: %.1f%%' % accuracy(
			predictions, train_labels[:train_subset, :]))
      		# Calling .eval() on valid_prediction is basically like calling run(), but
      		# just to get that one numpy array. Note that it recomputes all its graph
      		# dependencies.
			print('Validation accuracy: %.1f%%' % accuracy(
				valid_prediction.eval(), valid_labels))
	print('Test accuracy: %.1f%%' % accuracy(test_prediction.eval(), test_labels))


t1 = time.time()
elapsed_time = t1-t0 
print("Time to run the computation graph (gradient descent) = ", str(elapsed_time), " sec.")
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
'''




# ---------------------------------------------------------------------------------
# Construct the computation graph: Stochastic gradient descent method. 
# ---------------------------------------------------------------------------------
t0 = time.time()


graph = tf.Graph()
with graph.as_default():

	# Input data. For the training data, we use a placeholder that will be fed
	# at run time with a training minibatch.
	tf_train_dataset = tf.placeholder(tf.float32,
                                    shape=(gv.batch_size, gv.image_size * gv.image_size))
	tf_train_labels = tf.placeholder(tf.float32, shape=(gv.batch_size, gv.num_labels))
	tf_valid_dataset = tf.constant(valid_dataset)
	tf_test_dataset = tf.constant(test_dataset)
  
	# Variables.
	weights = tf.Variable(
		tf.truncated_normal([gv.image_size * gv.image_size, gv.num_labels], stddev=0.1))
	biases = tf.Variable(tf.zeros([gv.num_labels]))
  
	# Training computation.
	logits = tf.matmul(tf_train_dataset, weights) + biases
	loss = tf.reduce_mean(
		tf.nn.softmax_cross_entropy_with_logits(logits, tf_train_labels))
  
	# Optimizer.
	optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss)
  
	# Predictions for the training, validation, and test data.
	train_prediction = tf.nn.softmax(logits)
	valid_prediction = tf.nn.softmax(
		tf.matmul(tf_valid_dataset, weights) + biases)
	test_prediction = tf.nn.softmax(tf.matmul(tf_test_dataset, weights) + biases)


t1 = time.time() 
elapsed_time = t1-t0
print('Time to construct computation graph (stochastic gradient descent) = ', str(elapsed_time), " sec.")
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------

# ---------------------------------------------------------------------------------
# RUN THE GRAPH COMPUTATION: Stochastic gradient descent method. 
# ---------------------------------------------------------------------------------
num_steps = 100001

tfsession.run_tf_session_SGD(num_steps, graph, gv.batch_size, train_dataset, train_labels, valid_labels, test_labels, 
	tf_train_dataset, tf_train_labels, train_prediction, valid_prediction, test_prediction, optimizer, loss)

