# ------------------------------------------------------------------------------------------------------------------------------
# Assignment 2: Modify existing code to train 1-hidden neural network with 1024 RELUs using stochastic gradient descent. 
# ------------------------------------------------------------------------------------------------------------------------------


# ----------
# FINDINGS:
# ----------
# We get vastly improved results compared to the plain logistic regression model. 
# The maximum possible accuracy for validation ~ 89%  and for test ~94% 
# This does not include regularization. We only varying learning rate and number of epochs. 


from __future__ import print_function
import numpy as np
import tensorflow as tf 
from six.moves import cPickle as pickle
from six.moves import range

import time
import sys

sys.path.insert(1, '../helperfiles/')


import global_variables as gv
import datafiles as df 
import tfsession

# ---------------------------------------------------------------------------------
# Loading data stored in .pickle format in Assignment 1. 
# ---------------------------------------------------------------------------------
train_dataset, train_labels, valid_dataset, valid_labels, test_dataset, test_labels = df.load_data(gv.pickle_file)
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------


# Reformatting data into shape that is amenable to train. 
# ---------------------------------------------------------------------------------
t0 = time.time()

train_dataset, train_labels = df.reformat(train_dataset, train_labels, gv.image_size, gv.num_labels)
valid_dataset, valid_labels = df.reformat(valid_dataset, valid_labels, gv.image_size, gv.num_labels)
test_dataset, test_labels = df.reformat(test_dataset, test_labels, gv.image_size, gv.num_labels)
print('Training set', train_dataset.shape, train_labels.shape)
print('Validation set', valid_dataset.shape, valid_labels.shape)
print('Test set', test_dataset.shape, test_labels.shape)

t1 = time.time()
elapsed_time = t1-t0 
print("Time to reformat data = ", str(elapsed_time), " sec.")
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------
# Construct the computation graph: Stochastic gradient descent method. 
# ---------------------------------------------------------------------------------
numnodes_hidden_layer = 1024

def neural_network_components():
	# CONSTRUCTION OF FEED FORWARD NETWORK
	# Input image ==> Hidden layer 
	hidden_layer = {'weights': tf.Variable(
		tf.truncated_normal([gv.image_size * gv.image_size, numnodes_hidden_layer], stddev=0.1)), 
	'biases': tf.Variable(tf.zeros([numnodes_hidden_layer]))}


	# Hidden layer ==> Output layer
	output_layer = {'weights': tf.Variable(
		tf.truncated_normal([numnodes_hidden_layer, gv.num_labels], stddev=0.1)), 
	'biases': tf.Variable(tf.zeros([gv.num_labels]))}

	return hidden_layer, output_layer


def assemble_neural_network(hidden_layer, output_layer, dataset):
	
	hidden_layer_output = tf.add(tf.matmul(dataset, hidden_layer['weights']), 
		hidden_layer['biases'])

	hidden_layer_output = tf.nn.relu(hidden_layer_output)

	output_layer_output = tf.add(tf.matmul(hidden_layer_output, output_layer['weights']), 
		output_layer['biases'])

	return output_layer_output

t0 = time.time()


graph = tf.Graph()
with graph.as_default():

	# Input data. For the training data, we use a placeholder that will be fed
	# at run time with a training minibatch.
	tf_train_dataset = tf.placeholder(tf.float32,
                                    shape=(gv.batch_size, gv.image_size * gv.image_size))
	tf_train_labels = tf.placeholder(tf.float32, shape=(gv.batch_size, gv.num_labels))
	tf_valid_dataset = tf.constant(valid_dataset)
	tf_test_dataset = tf.constant(test_dataset)
  
	# 1. Construct computation graph: define components of the feedforward network. 
	hidden_layer, output_layer = neural_network_components()
	

	# ------------
	# TRAINING: 
	# ------------

	# 2. Construct computation graph: define how each layer is assembled into 
	#                                 the required feedforward network. 
	logits_train = assemble_neural_network(hidden_layer, output_layer, tf_train_dataset) 


	# 3. Training computation.
	loss = tf.reduce_mean(
		tf.nn.softmax_cross_entropy_with_logits(logits_train, tf_train_labels)) 
  
	# 4. Optimizer.
	# NOTES: 

	optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss)
  	
	# > Lowering the rate further (0.0025) and running for more number of steps (num_steps = 4001),
	#    gets us 90.9% accuracy, a slight improvement. 
	#    optimizer = tf.train.GradientDescentOptimizer(0.0025).minimize(loss)

	# 5. Predictions for the training, validation, and test data.
	train_prediction = tf.nn.softmax(logits_train)
	

	# ----------------------------------------------------------------
	# Once weights of individual layer are trained, 
	# we run the trained network for validation set and test set. 
	# ----------------------------------------------------------------
	
	# -----------
	# Validation: 
	# -----------
	logits_valid = assemble_neural_network(hidden_layer, output_layer, tf_valid_dataset) 
	valid_prediction = tf.nn.softmax(logits_valid)
	# -----------
	# Testing: 
	# -----------
	logits_test = assemble_neural_network(hidden_layer, output_layer, tf_test_dataset) 
	test_prediction = tf.nn.softmax(logits_test)


t1 = time.time() 
elapsed_time = t1-t0
print('Time to construct computation graph (stochastic gradient descent) = ', str(elapsed_time), " sec.")
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------

# ---------------------------------------------------------------------------------
# RUN THE GRAPH COMPUTATION: Stochastic gradient descent method. 
# ---------------------------------------------------------------------------------
num_steps = 6001

tfsession.run_tf_session_SGD(num_steps, graph, gv.batch_size, train_dataset, train_labels, valid_labels, test_labels, 
	tf_train_dataset, tf_train_labels, train_prediction, valid_prediction, test_prediction, optimizer, loss)

