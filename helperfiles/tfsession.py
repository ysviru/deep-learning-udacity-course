# Running computation graph in tensorflow 

# For different method such simple logistic regression, neural networks with RELUs, etc., 
# running the computational graph is the same and hence refactored into this python file. 


import tensorflow as tf 
import time
import datafiles as df 

def run_tf_session_SGD(num_steps, graph, batch_size, train_dataset, train_labels, valid_labels, test_labels, tf_train_dataset, tf_train_labels, train_prediction, valid_prediction, test_prediction, optimizer, loss): 

	# ---------------------------------------------------------------------------------
	# RUN THE GRAPH COMPUTATION: Stochastic gradient descent method. 
	# ---------------------------------------------------------------------------------
	t0 = time.time() 

	with tf.Session(graph=graph) as session:
		tf.global_variables_initializer().run()
		print("Initialized")
		for step in range(num_steps):
			# Pick an offset within the training data, which has been randomized.
			# Note: we could use better randomization across epochs.
			offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
			# Generate a minibatch.
			batch_data = train_dataset[offset:(offset + batch_size), :]
			batch_labels = train_labels[offset:(offset + batch_size), :]
			# Prepare a dictionary telling the session where to feed the minibatch.
			# The key of the dictionary is the placeholder node of the graph to be fed,
			# and the value is the numpy array to feed to it.
			feed_dict = {tf_train_dataset : batch_data, tf_train_labels : batch_labels}
			_, l, predictions = session.run(
				[optimizer, loss, train_prediction], feed_dict=feed_dict)
			if (step % 500 == 0):
				print("Minibatch loss at step %d: %f" % (step, l))
				print("Minibatch accuracy: %.1f%%" % df.accuracy(predictions, batch_labels))
				print("Validation accuracy: %.1f%%" % df.accuracy(
					valid_prediction.eval(), valid_labels))
		print("Test accuracy: %.1f%%" % df.accuracy(test_prediction.eval(), test_labels))

	t1 = time.time()
	elapsed_time = t1-t0
	print("Time to run the computation graph (stochastic gradient descent) = ", str(elapsed_time), " sec.")




