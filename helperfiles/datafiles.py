# Helper routine to load image data. 

# ---------------------------------------------------------------------------------
# Loading data stored in .pickle format in Assignment 1. 
# ---------------------------------------------------------------------------------
import time
from six.moves import cPickle as pickle
import numpy as np

def load_data(pickle_file):
	t0 = time.time()
	
	with open(pickle_file, 'rb') as f:
		save = pickle.load(f)
		train_dataset = save['train_dataset']
		train_labels = save['train_labels']
		valid_dataset = save['valid_dataset']
		valid_labels = save['valid_labels']
		test_dataset = save['test_dataset']
		test_labels = save['test_labels']
		del save  # hint to help gc free up memory
		print('Training set', train_dataset.shape, train_labels.shape)
		print('Validation set', valid_dataset.shape, valid_labels.shape)
		print('Test set', test_dataset.shape, test_labels.shape)

	t1 = time.time()
	elapsed_time = t1-t0 
	print('Time to load data = ', str(elapsed_time), " sec.")

	return train_dataset, train_labels, valid_dataset, valid_labels, test_dataset, test_labels

# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------
# Reformatting data into shape that is amenable to train. 
# ---------------------------------------------------------------------------------

def reformat(dataset, labels, image_size, num_labels):
	dataset = dataset.reshape((-1, image_size * image_size)).astype(np.float32)
	# Map 0 to [1.0, 0.0, 0.0 ...], 1 to [0.0, 1.0, 0.0 ...]
 	labels = (np.arange(num_labels) == labels[:,None]).astype(np.float32)
	return dataset, labels
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------





# ------------------
# Accuracy function 
# ------------------
def accuracy(predictions, labels):
	return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
          / predictions.shape[0])
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------


